import java.util.Date;

public class BabyWellness extends Voucher {

    private int age;
    private String birthCertificate;
    private String gender;

    public BabyWellness(int id_voucher, String surname, String name, int number, String typeHousing, String dateArrival, String dateDepartureccc, int count, int price, int age, String birthCertificate, String gender) {
        super(id_voucher, surname, name, number, typeHousing, dateArrival, dateDepartureccc, count, price);
        this.age = age;
        this.birthCertificate = birthCertificate;
        this.gender = gender;
    }

    public String toString() {
        return super.toString() + "\n" + "BabyWellness:" + "\n" + "age = " + age + "\n" + "birthCertificate = " + birthCertificate + "\n" + "gender = " + gender;
    }


}
