import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Voucher> voucherArrayList = new ArrayList<>();

        BabyWellness babyWellness = new BabyWellness(1, "Ivan", "Hot", 456, "One", "12.05.2020", "15.05.2020", 7, 5000, 10, "zex", "m");
        voucherArrayList.add(babyWellness);

        OverseasTrips overseasTrips = new OverseasTrips(2, "Jon", "Cold", 654, "two", "15.12.2019", "20.12.2019", 65, 4500, "internat", "asdxz");
        voucherArrayList.add(overseasTrips);

        Sanatoriums sanatoriums = new Sanatoriums(3, "Tom", "Weather", 852, "four", "25.05.2020", "27.05.2020", 45, 7500, "medpol", "ystal", "recomend");
        voucherArrayList.add(sanatoriums);

        for (Voucher voucher : voucherArrayList) {
            System.out.println(voucher + "\n");
        }


    }

}
