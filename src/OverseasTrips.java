import java.util.Date;

public class OverseasTrips extends Voucher {

    private String internationalPassport;
    private String insurance;

    public OverseasTrips(int id_voucher, String surname, String name, int number, String typeHousing, String dateArrival, String dateDepartureccc, int count, int price, String internationalPassport, String insurance) {
        super(id_voucher, surname, name, number, typeHousing, dateArrival, dateDepartureccc, count, price);
        this.internationalPassport = internationalPassport;
        this.insurance = insurance;
    }

    public String toString() {
        return super.toString() + "\n" + "OverseasTrips: " + "\n" + "internationalPassport = " + internationalPassport + "\n" + "insurance = " + insurance;

    }


}


