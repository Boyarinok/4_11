import java.util.Date;

public class Sanatoriums extends Voucher {

    private String medicalPolicy;
    private String diagnosis;
    private String direction;

    public Sanatoriums(int id_voucher, String surname, String name, int number, String typeHousing, String dateArrival, String dateDepartureccc, int count, int price, String medicalPolicy, String diagnosis, String direction) {
        super(id_voucher, surname, name, number, typeHousing, dateArrival, dateDepartureccc, count, price);
        this.medicalPolicy = medicalPolicy;
        this.diagnosis = diagnosis;
        this.direction = direction;
    }

    public String toString() {
        return super.toString() + "\n" + "Sanatoriums:" + "\n" + "medicalPolicy = " + medicalPolicy + "\n" + "diagnosis = " + diagnosis + "\n" + "direction = " + direction;
    }

}
