import java.util.Date;

public class Voucher {

    private int id_voucher;
    private String surname;
    private String name;
    private int number;
    private String typeHousing;
    private String dateArrival;
    private String dateDepartureccc;
    private int count;
    private int price;

    public Voucher(int id_voucher, String surname, String name, int number, String typeHousing, String dateArrival, String dateDepartureccc, int count, int price) {
        this.id_voucher = id_voucher;
        this.surname = surname;
        this.name = name;
        this.number = number;
        this.typeHousing = typeHousing;
        this.dateArrival = dateArrival;
        this.dateDepartureccc = dateDepartureccc;
        this.count = count;
        this.price = price;
    }

    public String toString() {
        return "Voucher:" + "\n" + "id_voucher = " + id_voucher + "\n" + "surname = " + surname + "\n" + "name = " + name + "\n" + "typeHousing = " + typeHousing + "\n" + "dateArrival = " + dateArrival + "\n" + "dateDepartureccc = " + dateDepartureccc + "\n" + "count = " + count + "\n" + "price = " + price;
    }

}
